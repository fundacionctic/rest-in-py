`REST in python (RESTinpy)`_ (0.1.0)
====================================

A Python library to ease the publication of REST-style web services in
`Django`_ applications, specially (but not exclusively) those using the
Django Model framework. The package is licensed under `LGPL v3
license`_, and it can be downloaded in ``.zip`` and ``.tar.gz`` formats
`from Bitbucket`_.

.. image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/download.png
   :target: https://bitbucket.org/fundacionctic/rest-in-py/downloads

`Documentation`_ is available in the wiki.

Dependencies
------------

-  for JSON, the `simplejson`_ package
-  `mimeparse`_ package

These packages are imported in a lazy fashion, i.e., only when needed.
Therefore, if the user never intends to use the JSON format, the
``simplejson`` package is not imported and the user does not have to
install it.

Development Team
----------------

-  `Diego Berrueta`_ (`Fundación CTIC`_), |berrueta-foaf|
-  `Carlos Tejo`_ (`Fundación CTIC`_), |dayures-foaf|
-  Jana Álvarez (`Fundación CTIC`_)
-  `Sergio Fernández`_ (`Fundación CTIC`_), |wikier-foaf|


Project details
---------------

The project RESTinpy is hosted in `Bitbucket`_, where you can find
more details about it.

More information
----------------

For more information, contact us :

- `Carlos Tejo <https://bitbucket.org/carlos_tejo>`__
- `Luis Polo <https://bitbucket.org/luis_polo>`__

Acknowledgements
----------------

RESTinpy has been developed by CTIC Foundation as a by-product of
`WodiDoc`_, the main result of WP7 of the `Vulcano`_ project. Vulcano is
partially funded by the Spanish Ministry of Industry, Tourism and
Commerce under the National Plan for Scientific Research, Development
and Technological Innovation 2007-2010 (grant number
TSI-020301-2008-22).

|ctic| |vulcano| |mityc|

.. _REST in python (RESTinpy): Bitbucket
.. _Django: http://www.djangoproject.com/
.. _LGPL v3 license: http://www.gnu.org/copyleft/lesser.html
.. _from Bitbucket: https://bitbucket.org/fundacionctic/rest-in-py
.. _Documentation: https://bitbucket.org/fundacionctic/rest-in-py/wiki
.. _simplejson: http://undefined.org/python/#simplejson
.. _mimeparse: http://code.google.com/p/mimeparse/
.. _Diego Berrueta: http://berrueta.net/
.. _Fundación CTIC: http://www.fundacionctic.org/
.. _Carlos Tejo: http://www.dayures.net/
.. _Sergio Fernández: http://www.wikier.org/
.. _Bitbucket: https://bitbucket.org/fundacionctic/rest-in-py
.. _WodiDoc: http://wodidoc.morfeo-project.org/
.. _Vulcano: http://www.ines.org.es/vulcano/

.. |berrueta-foaf| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/foaf.gif
                                             :target: http://berrueta.net/foaf.rdf#me

.. |dayures-foaf| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/foaf.gif
                  :target: http://www.dayures.net/foaf.rdf#me

.. |wikier-foaf| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/foaf.gif
                 :target: http://www.wikier.org/foaf#wikier

.. |ctic| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/ctic.png
   :target: http://www.fundacionctic.org/

.. |vulcano| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/vulcano.png
   :target: http://www.ines.org.es/vulcano

.. |mityc| image:: https://bitbucket.org/fundacionctic/rest-in-py/raw/master/webpage/resources/images/mityc.gif
   :target: http://www.mityc.es/
