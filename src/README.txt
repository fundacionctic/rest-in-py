
REST-in-py: restify your Django applications
--------------------------------------------

The distribution contains:

- RESTinpy: the Python library. You should copy the directory 
  somewhere into your PYTHONPATH. Alternatively, you can also run 
  the setuptools scrips:

    python setup.py install
  
Please check the installation manual:

   http://apps.sourceforge.net/mediawiki/rest-in-py/index.php?title=Installation

   
Please check the project web page for further information,
detailed installation instructions and usage documentation:

   http://rest-in-py.sourceforge.net/
   

