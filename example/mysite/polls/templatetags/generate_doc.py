# * coding: utf-8*

# (c) Copyright Fundacion CTIC, 2009
# http://www.fundacionctic.org/
#
# GNU General Public Licence (GPL)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""filters"""

from django.utils.translation import ugettext as _
from django import template
from django.db.models.fields import *
from django.db.models.fields.related import *
from django.db.models.fields.files import ImageField
from RESTinpy.model_resources import ModelResource, ModelListResource

register = template.Library()

simpleFields = {
  BooleanField: _("Boolean"),
  CharField: _("String"),
  DateTimeField: _("Datetime"),
  EmailField: _("Email address"),
  ImageField: _("Image"),
  SlugField: _("Slug"),
  URLField: _("URL"),
}

simpleFieldsDoc = {
  BooleanField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#booleanfield",
  CharField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#charfield",
  DateTimeField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#datetimefield",
  EmailField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#emailfield",
  ImageField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#imagefield",
  SlugField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#slugfield",
  URLField: "http://docs.djangoproject.com/en/dev/ref/models/fields/#urlfield",
}  

@register.filter("get_class")
def get_class(field, resource_universe=None):

    if field.__class__ in simpleFields:
        return simpleFields[field.__class__]
    elif field.__class__ is ForeignKey or field.__class__ is ManyToManyField:
        referenced_resource = get_referenced_resource(field, resource_universe)
        if referenced_resource:
            return 'Reference to resource %s' % referenced_resource.name
        else:
            return "Reference to model %s" % field.rel.to
    else:
        return field.__class__#dir(field)#.__dict__

@register.filter("get_link")
def get_link(field, resource_universe=None):
    if field.__class__ in simpleFields:
        return simpleFieldsDoc[field.__class__]
    elif field.__class__ is ForeignKey or field.__class__ is ManyToManyField:
        referenced_resource = get_referenced_resource(field, resource_universe)
        if referenced_resource:
            return '#%s' % referenced_resource.name
        else:
            return "#"
    else:
        return "#"
        
def get_referenced_resource(field, resource_universe):
    candidates = filter(lambda r: r.model_class is field.rel.to and ((isinstance(r, ModelResource) and field.__class__ is ForeignKey) or (isinstance(r, ModelListResource) and field.__class__ is ManyToManyField)), resource_universe)
    return candidates[0] if candidates else None