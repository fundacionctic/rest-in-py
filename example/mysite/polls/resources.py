from RESTinpy import model_resources
from mysite.polls import models

#from django.contrib.auth.models import User
# Fallback_get
#from django.views.generic.list_detail import object_detail, object_list 

class PollsResource(model_resources.ModelListResource):
    """This resource represents the collection of all the polls registered in the system."""
    
    def __init__(self, name, **kwargs):
        model_resources.ModelListResource.__init__(self, name, ('GET', ),models.Poll, **kwargs )
        # Fallback_get
        # self.fallback_get = object_list

class PollResource(model_resources.ModelResource):
    """This resource represents a poll"""
    
    def __init__(self, name, **kwargs):
        model_resources.ModelResource.__init__(self, name, ('GET', ),models.Poll, **kwargs)


class ChoicesResource(model_resources.ModelListResource):
    """This resource represents the collection of choices associated to a poll"""

    def __init__(self, name, **kwargs):
        model_resources.ModelListResource.__init__(self, name, ('GET', ),models.Choice, **kwargs)

class ChoiceResource(model_resources.ModelResource):
    """This resource represents a choice associated to a poll"""

    def __init__(self, name, **kwargs):
        model_resources.ModelResource.__init__(self, name, ('GET', ), models.Choice, **kwargs)

# Create an object of each one
polls_resource = PollsResource('polls')
poll_resource  = PollResource ('poll', parent_resource = polls_resource)
choices_resource = ChoicesResource('choices')
choice_resource  = ChoiceResource ('choice' , parent_resource = choices_resource)

def list_local_resources():
    return [polls_resource, poll_resource, choices_resource, choice_resource]