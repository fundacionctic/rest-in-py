This example is an extension of the example explained in Django Tutorial [1]

Per each django application:
- Create a new file resources.py
    � Define a class per each resource that you want to expose. 
    . If the resource is a single resource, it should inherites from RESTinpy.model_resources.ModelResource
    . If the resource is a list of resources, it should inherites from RESTinpy.model_resources.ModelListResource 
    � In the __init__ method you can configurate the allowed methods ("('GET', 'PUT')"), the class to expose ("models.Poll")
- In urls.py, add the lines between #--- Added --- and #--- end-Added ---

[1] http://docs.djangoproject.com/en/dev/intro/tutorial01/