#from django.contrib import admin
from django.conf.urls.defaults import *

#admin.autodiscover()

urlpatterns = patterns('',
    (r'^polls/', include('mysite.polls.urls')),
)

#--- Added ---
from RESTinpy import formatters, adapters
from RESTinpy.resources import render_info
from mysite.polls import resources
import logging

all_resources = resources.list_local_resources()
logging.debug ("The following resources have been detected: %s" % all_resources)

uri_adapter = adapters.UriAdapter(all_resources, baseurl = "http://localhost:8000/polls")
directory_uri_adapter = adapters.DirectoryUriAdapter(all_resources, baseurl = "http://localhost:8000/polls")

for resource in all_resources:
    logging.debug("Configuring resource %s" % resource)
    resource.adapters = [uri_adapter, directory_uri_adapter]
    resource.formatters = {'application/xml' : formatters.XmlFormatter(), 'application/json' : formatters.JsonFormatter()} # default formatters;
    resource.fallback_mimetypes = ['text/html', 'application/xhtml+xml']
    urlpatterns += resource.get_urlpatterns()
    
urlpatterns += patterns('',
  (r'^api-doc/$', render_info, {'resources': all_resources} ),
)

#Fallback_get
# urlpatterns += patterns('', (r'^polls/$', 'django.views.generic.list_detail.object_list', info_dict), )

#--- end-Added ---
